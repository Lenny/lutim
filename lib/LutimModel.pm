package LutimModel;
use Mojolicious;
use FindBin qw($Bin);
use File::Spec qw(catfile);
use Mojo::Pg;

BEGIN {
    my $m = Mojolicious->new;
    our $config = $m->plugin('Config' =>
        {
            file    => File::Spec->catfile($Bin, '..' ,'lutim.conf'),
            default => {
                db_path => 'lutim.db'
            }
        }
    );
}

my $c = shift;
my $pg = Mojo::Pg->new('postgresql://localhost/lutimDB');
#$pg->password($c->config->{pgdb}->{pwd});
#$pg->username($c->config->{pgdb}->{user});

$pg->migrations->from_string(<<EOF)->migrate;
-- 1 up
CREATE TABLE lutim (
               short                 TEXT PRIMARY KEY,
               path                  TEXT,
               footprint             TEXT,
               enabled               INTEGER,
               mediatype             TEXT,
               filename              TEXT,
               counter               INTEGER,
               delete_at_first_view  INTEGER,
               delete_at_day         INTEGER,
               created_at            INTEGER,
               created_by            TEXT,
               last_access_at        INTEGER,
               mod_token             TEXT,
               width                 INTEGER,
               height                INTEGER);
-- 1 down
DROP TABLE lutim;
EOF

1;
